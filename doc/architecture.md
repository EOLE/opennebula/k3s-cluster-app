# K3S Cluster appliance architecture



## Option 1 : K3S with a VIP

```mermaid
graph TD
  %% Networks
  Pub((Public))
  Int((Internal))
  Neb((Eole))

  %% Network devices
  Fw([Firewall])
  VIP{{VIP}}
  Vr([vRouter])

  %% K3S nodes
  l1([Leader])
  s0([Server 0])
  s1([Server 1])
  a0([Agent 0])
  a1([Agent 1])
  a2([Agent 2])
  a3([Agent 3])

  Int --- Vr ---  Neb

  subgraph k3s [K3S Cluster]
    style k3s fill:#111,stroke:#aaa,stroke-width:4px
    subgraph Servers
      l1 & s0 & s1
    end
    subgraph Agents
      a0 & a1 & a2 & a3
    end
  end

  Int --- l1 & s0 & s1 & a0 & a1 & a2 & a3
  l1 & s0 & s1 --- VIP

  VIP --- Fw --- Pub


```


### Networks
The cluster appliance needs two networks :
* Public network (Public)
* Cluster internal network (Internal)
* Eole network (Eole)

The public network gives access to Internet and have to be accessible by the end users.
The cluster internal network is for the cluster internal communication this network have to be totaly isolated.
The Eole network gives accès to the OneGate server and the  rest of the world to the cluster, the default gateway must be the vRouter

### Virtual IP (VIP)

The cluster needs a virtual IP this is a public IP transfered to the current working loadbalancer node

## Option 2 : The Reverse proxy way

```mermaid
graph TD
  rp([Reverse proxy])
  vRouter([vRouter])

  Int((Internal))
  Pub((Public))
  Nebula((EOLE))

  l1([Leader])
  a0([Agent 0])
  a1([Agent 1])
  a2([Agent 2])
  a3([Agent 3])

  subgraph Eole
     vRouter --- Nebula
  end
  subgraph k3s [K3S cluster]
    style k3s fill:#111,stroke:#aaa,stroke-width:4px
    l1 & a0 & a1 & a2 & a3
  end
  Int --- l1 & a0 & a1 & a2 & a3

  subgraph Public
    rp --- Pub
  end
  Int --- rp
  Int --- vRouter
```

### Networks
The cluster appliance needs two networks :
* Public network (Public)
* Cluster internal network (Internal)
* Eole network (Eole)

The public network gives access to Internet and have to be accessible by the end users.
The cluster internal network is for the cluster internal communication this network have to be totaly isolated.
The Eole network gives accès to the OneGate server and the  rest of the world to the cluster, the default gateway must be the vRouter

### The reverse proxy

This "service" provides a simple way to give access to the kubernetes cluster, without any complicated configuration.
The reverse proxy pass the "http" requests to the cluster members and that's it, no VIP, no complications.