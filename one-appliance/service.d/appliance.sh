#!/usr/bin/env bash

# ---------------------------------------------------------------------------- #
# Copyright 2018-2021, OpenNebula Project, OpenNebula Systems                  #
#                                                                              #
# Licensed under the Apache License, Version 2.0 (the "License"); you may      #
# not use this file except in compliance with the License. You may obtain      #
# a copy of the License at                                                     #
#                                                                              #
# http://www.apache.org/licenses/LICENSE-2.0                                   #
#                                                                              #
# Unless required by applicable law or agreed to in writing, software          #
# distributed under the License is distributed on an "AS IS" BASIS,            #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     #
# See the License for the specific language governing permissions and          #
# limitations under the License.                                               #
# ---------------------------------------------------------------------------- #

# Important notes #############################################################
#
# This appliance does nothing, it's just a template to create new ones.
#
# Important notes #############################################################

# List of contextualization parameters
ONE_SERVICE_PARAMS=(
  'ONEAPP_PARAM' 'configure' 'A parameter' 'M|boolean'
)

### Appliance metadata ########################################################

ONE_SERVICE_NAME='K3S cluster'
ONE_SERVICE_VERSION=0.1
ONE_SERVICE_BUILD=$(date +%s)
ONE_SERVICE_SHORT_DESCRIPTION='Start a full K3S cluster in your Nebula infrastructure'
ONE_SERVICE_DESCRIPTION=$(
  cat <<EOF
Appliance which starts a K3S cluster.

Initial configuration can be customized via parameters:

$(params2md 'configure')

EOF
)

ONEGATE_DATA=""

### Contextualization defaults ################################################

### Globals ###################################################################

DEP_PKGS="
    ca-certificates
    coreutils
    curl
    wget
    haproxy
    jq
    openssh-server
    openssl
    k3s
    dnsmasq
    pwgen
    open-iscsi
    bash-completion
"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#
# service implementation
#

service_cleanup() {
  return 0
}

service_install() {

  # packages
  install_pkgs ${DEP_PKGS}

  # update K3S binary with the last version
  # DEPRECATED FOR NOW
  # update_k3s

  # Fix flannel cni plugin installation if needed
  check_flannel_cni

  # Install helm tool from edge
  install_helm

  # Perpare server for K3S
  prepare_server

  # service metadata
  create_one_service_metadata

  # cleanup
  postinstall_cleanup

  msg info "INSTALLATION FINISHED"

  return 0
}

service_configure() {

  msg info "CONFIGURATION FINISHED"
  return 0
}

service_bootstrap() {
  setup_daemon
  start_daemon
  setup_onegate

  msg info "BOOTSTRAP FINISHED"
  return 0
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#
# functions
#

check_flannel_cni() {
  # On alpine 3.16, cni-plugin packages don't install flannel but flannel-amd64
  # But k3s only check if flannel exists...
  if [ ! -e /usr/libexec/cni/flannel ] && [ -e /usr/libexec/cni/flannel-amd64 ]; then
    ln -s /usr/libexec/cni/flannel-amd64 /usr/libexec/cni/flannel
  fi
}

install_helm() {
  set +e
  msg info "Installing helm"
  cmd=$(which helm)
  if [ "${?}" -ne 0 ]; then
    _REPO="http://dl-cdn.alpinelinux.org/alpine/edge/testing"
    apk add helm --repository="${_REPO}"
  fi
  set -e
}

# DEPRECATED (for now)
update_k3s() {
  msg info "Updating k3s"
  # Updating k3s
  RELEASE="v1.19.16+k3s1"
  K3D_BIN="https://github.com/k3s-io/k3s/releases/download/${RELEASE}/k3s"
  DST="/usr/bin/k3s"

  k3sVersion=$(k3s --version | awk '{print $2}')
  if [ "${k3sVersion}" == "${RELEASE}" ]; then
    return 0
  fi
  curl ${K3D_BIN} --output ${DST}
  if [ "${?}" -eq 0 ]; then
    chmod +x ${DST}
  fi
}

prepare_server() {
  # Prepare host for k3s
  sed -i -e 's/default_kernel_opts="quiet rootfstype=ext4"/default_kernel_opts="noquiet rootfstype=ext4 cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory"/g' \
    /etc/update-extlinux.conf

  update-extlinux

  echo 'rc_after="dbus"' >/etc/conf.d/one-context
  rc-update add cgroups boot
  rc-update add dbus boot
  rc-update del one-context boot
  rc-update add one-context default
  #Do not enable k3s now
  #rc-update add k3s default
  rc-update add local default

  # Configure k3s to use our proxyhub instead of the docker.io registry
  mkdir -p /etc/rancher/k3s/
  cat >/etc/rancher/k3s/registries.yaml <<EOF
mirrors:
  "hub.eole.education":
    endpoint:
      - "https://hub.eole.education"
EOF

  mount --make-rshared /
}

setup_restart_daemon() {
  ROLE_NAME=${1}
  LOCAL_ACTION_SCRIPT="/etc/local.d/k3s.start"
  SETUP_OPENEBS="/etc/local.d/openebs.start"
  # K3S have to be restarted on first boot

  if [ "${ROLE_NAME}" = "reverseProxy" ]; then
    # Nothing to do for now
    return
  fi

  cat >${LOCAL_ACTION_SCRIPT} <<EOF
#!/bin/sh

mount --make-rshared /

FLAG="/var/run/k3sapp.done"

if [ ! -f "${FLAG}" ]; then
  service k3s restart && touch ${FLAG}
fi

exit 0
EOF
  chmod +x "${LOCAL_ACTION_SCRIPT}"

  if [ "${ROLE_NAME}" = "leader" ]; then
    cat >${SETUP_OPENEBS} <<EOF
#!/bin/sh

FLAG="/var/run/openebs.done"

if [ ! -f "${FLAG}" ]; then
  sh /etc/one-appliance/service.d/postdeploy.sh && touch ${FLAG}
fi

exit 0
EOF
    chmod +x ${SETUP_OPENEBS}
  fi
}

setup_agent() {
  K3S_CONFIG="/etc/conf.d/k3s"

  _onegate_server_id=$(onegate service show --json | jq -c '.SERVICE.roles[]  | select(.name == "leader") | .nodes[0].deploy_id')
  _url=$(onegate vm show ${_onegate_server_id} --json | jq -cr .VM.USER_TEMPLATE.K3S_URL)
  _token=$(onegate vm show ${_onegate_server_id} --json | jq -cr .VM.USER_TEMPLATE.K3S_TOKEN)

  cat >${K3S_CONFIG} <<EOF
#k3s options
export PATH="/usr/libexec/cni/:\$PATH"
K3S_EXEC="agent"
K3S_OPTS="--server ${_url} --token='${_token}'"
K3S_LOGFILE="/var/log/k3s.log"
EOF
  rc-update add iscsid default
}

setup_server() {
  K3S_CONFIG="/etc/conf.d/k3s"
  PROVIDER="${1}"

  _onegate_server_id=$(onegate service show --json | jq -c '.SERVICE.roles[]  | select(.name == "leader") | .nodes[0].deploy_id')
  _url=$(onegate vm show ${_onegate_server_id} --json | jq -cr .VM.USER_TEMPLATE.K3S_URL)
  _token=$(onegate vm show ${_onegate_server_id} --json | jq -cr .VM.USER_TEMPLATE.K3S_TOKEN)

  K3S_OPTS=""
  if [ "${PROVIDER}" != "traefik" ]; then
    K3S_OPTS="--disable traefik"
  fi

  cat >${K3S_CONFIG} <<EOF
#k3s options
export PATH="/usr/libexec/cni/:\$PATH"
K3S_EXEC="server"
K3S_OPTS="${K3S_OPTS} --server ${_url} --token='${_token}'"
K3S_LOGFILE="/var/log/k3s.log"
EOF

  echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >>~root/.profile
}

init_cluster() {
  TOKEN_FILE="/var/lib/rancher/k3s/server/token"
  OPTS=""

  # Count NICs
  nicSize=$(onegate vm show --json | jq -cr ".VM.TEMPLATE.NIC | length")

  nicIdx=0
  if [ "${nicSize}" -eq 2 ]; then
    nicIdx=1
  fi

  # Get cluster IP
  tout=${1}
  _local_ip=$(onegate vm show --json | jq -cr .VM.TEMPLATE.NIC[${nicIdx}].IP)
  while [ "${_local_ip}" == "null" ]; do
    msg info "_local_ip is empty trying again"
    sleep 1
    _local_ip=$(onegate vm show --json | jq -cr .VM.TEMPLATE.NIC[${nicIdx}].IP)
    tout=$((tout - 1))
    if [ ${tout} -eq 0 ]; then
      msg error "Can't get local ip for server node..."
      return 4
    fi
  done

  if [ "${2}" != "traefik" ]; then
    OPTS="--disable traefik"
  fi

  msg info "Starting Cluster init"
  source /etc/conf.d/k3s
  # Do not use timeout here this have to be at least 120".
  timeout 120 k3s server --cluster-init ${OPTS} >>${K3S_LOGFILE} 2>&1
  if [ -e "${TOKEN_FILE}" ]; then
    ONEGATE_DATA="${ONEGATE_DATA}K3S_URL=https://${_local_ip}:6443\nK3S_TOKEN=$(cat ${TOKEN_FILE})\n"
    msg info "${ONEGATE_DATA}"
  else
    service k3s start
    sleep 10
    if [ -e "${TOKEN_FILE}" ]; then
      ONEGATE_DATA="${ONEGATE_DATA}K3S_URL=https://${_local_ip}:6443\nK3S_TOKEN=$(cat ${TOKEN_FILE})\n"
      msg info "${ONEGATE_DATA}"
    else
      msg error "Token file ${TOKEN_FILE} still missing !"
      return 2
    fi
  fi
  #mkdir -p /var/run/secrets/kubernetes.io/serviceaccount
  msg info "Cluster init ended"
  return 0
}

setup_leader() {
  K3S_CONFIG="/etc/conf.d/k3s"
  PROVIDER="${1}"

  K3S_OPTS=""
  if [ "${PROVIDER}" != "traefik" ]; then
    K3S_OPTS="--disable traefik"
  fi

  cat >${K3S_CONFIG} <<EOF
#k3s options
export PATH="/usr/libexec/cni/:\$PATH"
K3S_EXEC="server"
K3S_OPTS="${K3S_OPTS}"
K3S_LOGFILE="/var/log/k3s.log"
EOF

  echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >>~root/.profile
}

setup_dns() {
  msg info "Setting up DNS server"
  _agent_ids=$(onegate service show --json | jq -cr '.SERVICE.roles[] | select(.name == "agent") | .nodes[].deploy_id')
  msg info "${_agent_ids}"
  for agent in ${_agent_ids}; do
    _data=$(onegate vm show ${agent} --json)
    _agent_ip=$(echo ${_data} | jq -cr .VM.TEMPLATE.NIC[1].IP)
    _agent_name=$(echo ${_data} | jq -cr .VM.NODE_NAME)
    echo "# K3S Nodes " >>/etc/hosts
    echo "${_agent_ip} ${_agent_name}" >>/etc/hosts
  done
  rc-update add dnsmasq boot
}

wait_server_nodes() {
  tmout=${1}
  msg info "Waiting for all server nodes!"

  expected_nb_nodes=$(onegate service show --json | jq -rc '.SERVICE.roles[] | select(.name == "server") | .cardinality')

  while [ "${tmout}" -ne 0 ]; do
    sleep 1
    nb_nodes=$(onegate service show --json | jq -c '.SERVICE.roles[] | select(.name == "server") | .nodes | length')
    if [ "${nb_nodes}" -lt "${expected_nb_nodes}" ]; then
      tmout=$((tmout - 1))
      if [ "${tmout}" -eq 0 ]; then
        return 2
      fi
    else
      break
    fi
  done
}

setup_gw() {
  # Setup routing
  ip_ext=$(onegate vm show --json | jq -r '.VM.TEMPLATE.NIC[0].IP')
  iptables --table nat --append POSTROUTING --out-interface eth0 -j SNAT --to-source ${ip_ext}
  echo 1 >/proc/sys/net/ipv4/ip_forward

  # Setup internal DNS
  dns_domain=$(onegate vm show --json | jq -r '.VM.USER_TEMPLATE.DNS_DOMAIN')
  if [ "${dns_domain}" != "null" ]; then
    echo "address=/.${dns_domain}/${ip_ext}" >>/etc/dnsmasq.d/k3s.conf
  fi

  # Early start of DNS resolver
  msg info "Starting dnsmasq server early"
  service dnsmasq start
}

setup_rp() {
  HA_CONF="/etc/haproxy/haproxy.cfg"
  RSYSLOG_CONF="/etc/rsyslog.d/99-haproxy.conf"
  msg info "Setting up Haproxy"
  rc-update add haproxy default

  # Leader node should be up. We must wait server nodes to be up too.
  if ! wait_server_nodes 500; then
    msg error "Server nodes are not up. Haproxy conf will be incomplete."
  fi

  backend_ids=$(onegate service show --json | jq -c '.SERVICE.roles[]  | select((.name == "server") or (.name == "leader")) | .nodes[].deploy_id')

  cat >${HA_CONF} <<__EOF__
global
    log 127.0.0.1:514  local0
    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon
    stats socket /var/lib/haproxy/stats
listen stats
    bind :1936
    mode http
    timeout client 300s
    stats enable
    stats hide-version
    stats realm Haproxy\ Statistics
    stats uri /
defaults
    mode tcp
    timeout client 1m
    log global
    option tcplog
    option dontlognull
    option contstats
frontend main
    bind *:80
    bind *:443
    bind *:6443
    bind *:9000
    log global
    timeout client 30s
    maxconn 10000
    default_backend             app
backend app
    mode tcp
    balance     roundrobin
    log global
    timeout server 1m
    timeout connect 5s
__EOF__

  for bid in ${backend_ids}; do
    sname=server_${bid}
    ip=$(onegate vm show ${bid} --json | jq -rc .VM.TEMPLATE.NIC[0].IP)
    name="$(onegate vm show ${bid} --json | jq -rc .VM.NODE_NAME)"
    com="# ${name} : VM ${bid}"
    echo "    server ${sname} ${ip} check port 80 ${com}" >>${HA_CONF}
  done

  if [ ! -d $(dirname "${RSYSLOG_CONF}") ]; then
    mkdir -p $(dirname "${RSYSLOG_CONF}")
  fi

  cat >${RSYSLOG_CONF} <<__EOF__
# Collect log with UDP
module( load="imudp" TimeRegistry="500")
input(
  type="imudp"
  port="514"
)

# Creating separate log files based on the severity
local0.* /var/log/haproxy-traffic.log
local0.notice /var/log/haproxy-admin.log
__EOF__
}

setup_daemon() {
  msg info "Starting setup daemon"
  _onegate_vm_id=$(onegate vm show --json | jq -cr ".VM.ID" || true)
  if ! echo "$_onegate_vm_id" | grep -q '^[0-9]\+$'; then
    rc-update add k3s default
    msg info "No OneFlow support"
    return 0
  fi

  ROLE_NAME=$(onegate vm show --json | jq -cr .VM.USER_TEMPLATE.ROLE_NAME)
  INGRESS_PROVIDER=$(onegate vm show --json | jq -cr .VM.USER_TEMPLATE.INGRESS_PROVIDER)
  ONE_CONTEXT_CONFIG="/etc/conf.d/one-context"

  if [ "${ROLE_NAME}" = "leader" ]; then
    rc-update add k3s default
    setup_leader "${INGRESS_PROVIDER}"
    if [ ${?} -ne 0 ]; then
      msg error "Error configuring K3S leader"
      return 3
    fi
  fi

  if [ "${ROLE_NAME}" = "server" ]; then
    rc-update add k3s default
    setup_server "${INGRESS_PROVIDER}"
    if [ ${?} -ne 0 ]; then
      msg error "Error configuring K3S server"
      return 3
    fi
  fi

  if [ "${ROLE_NAME}" = "agent" ]; then
    rc-update add k3s default
    setup_agent
    if [ ${?} -ne 0 ]; then
      msg error "Error configuring K3S agent"
      return 4
    fi
  fi

  if [ "${ROLE_NAME}" = "reverseProxy" ]; then
    setup_gw # The reverseProxy is also the gateway for the internal network
    setup_rp
    if [ ${?} -ne 0 ]; then
      msg error "Error configuring Haproxy"
      return 5
    fi
  fi

  setup_restart_daemon ${ROLE_NAME}

}

start_daemon() {
  set +e
  ROLE_NAME=$(onegate vm show --json | jq -cr .VM.USER_TEMPLATE.ROLE_NAME)
  INGRESS_PROVIDER=$(onegate vm show --json | jq -cr .VM.USER_TEMPLATE.INGRESS_PROVIDER)
  _tmout=300

  setup_dns
  if [ ${?} -ne 0 ]; then
    msg error "Error configuring DNS server"
    return 2
  fi

  msg info "Starting dnsmasq server"
  service dnsmasq start
  if [ "${?}" -ne 0 ]; then
    return 3
  fi

  #  if [ "${ROLE_NAME}" =  "agent" ]; then
  #    msg info "Restarting k3s daemon"
  #    #service k3s restart & disown
  #  fi

  if [ "${ROLE_NAME}" = "leader" ]; then
    tout=120
    sleep 30

    # Init the cluster
    msg info "Cluster initialisation"
    init_cluster ${_tmout} ${INGRESS_PROVIDER}
    return ${?}
  fi

  if [ "${ROLE_NAME}" = "reverseProxy" ]; then
    msg info "Starting haproxy server"
    service haproxy start
    if [ "${?}" -ne 0 ]; then
      return 6
    fi
  fi

  set -e
}

setup_onegate() {
  _onegate_vm_id=$(onegate vm show --json | jq -cr ".VM.ID" || true)

  if ! echo "$_onegate_vm_id" | grep -q '^[0-9]\+$'; then
    msg info "No OneFlow support"
    return 0
  fi

  msg info "Set OneFlow service data..."

  ONEGATE_DATA="${ONEGATE_DATA}NODE_NAME=$(hostname)\n"

  msg info "Setup onegate data: ${ONEGATE_DATA}"
  if [ ! -z ${ONEGATE_DATA} ]; then
    onegate vm update --data ${ONEGATE_DATA}
  fi
}

postinstall_cleanup() {
  set +e
  msg info "Delete cache and stored packages"
  apk cache clean
  # we don't care if cache cleanup fail
  set -e
  return 0
}

install_pkgs() {
  msg info "Install required packages"
  if ! apk add --no-cache "${@}"; then
    msg error "Package(s) installation failed"
    exit 1
  fi
}
