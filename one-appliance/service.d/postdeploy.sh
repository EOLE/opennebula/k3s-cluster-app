#!/bin/sh

# ---------------------------------------------------------------------------- #
# ---------------------------------------------------------------------------- #

# Important notes #############################################################
#
# This script is not realy used by the appliance itself. This will be started
# by the rc.local mechanism, because it needs to be runned after the k3s service
# restart.
#
# Important notes #############################################################

export KUBECONFIG="/etc/rancher/k3s/k3s.yaml"
# shellcheck disable=SC2034
ONE_SERVICE_DIR=/etc/one-appliance
# shellcheck disable=SC2034
ONE_SERVICE_LOGDIR=/var/log/one-appliance
ONE_SERVICE_FUNCTIONS="${ONE_SERVICE_DIR}/service.d/functions.sh"

PROXYHUB="hub.eole.education"

# source this script's functions
# shellcheck disable=1090
. "$ONE_SERVICE_FUNCTIONS"

generateCSPCconf() {
  cat >"${1}" <<__EOF__
apiVersion: cstor.openebs.io/v1
kind: CStorPoolCluster
metadata:
  name: cstor-disk-pool
  namespace: openebs
spec:
  pools:
__EOF__

  for elm in $(kubectl get bd -n openebs | awk 'NR>1 {print $1":"$2}'); do
    bd=$(echo "$elm" | cut -d ':' -f 1)
    node=$(echo "$elm" | cut -d ':' -f 2)
    cat >>"${1}" <<_EOF_
    - nodeSelector:
        kubernetes.io/hostname: "${node}"
      dataRaidGroups:
        - blockDevices:
            - blockDeviceName: "${bd}"
      poolConfig:
        dataRaidGroupType: "stripe"
_EOF_
  done
}

# Generate CSPC yaml
generateStorageClassConf() {
  cat >"${1}" <<__EOF__
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: openebs-sparse-sc-statefulset
  annotations:
    openebs.io/cas-type: cstor
    cas.openebs.io/config: |
      - name: StoragePoolClaim
        value: "cstor-pool2"
      - name: ReplicaCount
        value: "3"
provisioner: openebs.io/provisioner-iscsi
__EOF__
}

# Generate cStor storage class (cstor-csi-disk.yaml)
generatecStorCSIDiskConf() {
  cat >${1} <<__EOF__
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: cstor-csi-disk
provisioner: cstor.csi.openebs.io
allowVolumeExpansion: true
parameters:
  cas-type: cstor
  # cstorPoolCluster should have the name of the CSPC
  cstorPoolCluster: cstor-disk-pool
  # replicaCount should be <= no. of CSPI created in the selected CSPC
  replicaCount: "3"
__EOF__
}

generateCertManagerConf() {
  _cert_email=$(onegate vm show --json | jq -rc '.VM.USER_TEMPLATE.LE_EMAIL')
  _cert_type="pki" # FIXME HERE
  _ingress=$(onegate vm show --json | jq -rc '.VM.USER_TEMPLATE.INGRESS_PROVIDER')
  if [ "${_ingress}" = "null" ]; then
    _ingress="traefik"
  fi

  if [ "${_cert_type}" = "pki" ]; then
    cat >"${1}" <<__EOF__
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigned-issuer
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    email: ${_cert_email}
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: prod-issuer-account-key
    solvers:
    - http01:
        ingress:
          class: ${_ingress}
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
spec:
  acme:
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    email: ${_cert_email}
    privateKeySecretRef:
      name: letsencrypt-staging
    solvers:
      - http01:
          ingress:
            class: ${_ingress}
__EOF__
  fi
}

generateCertificate() {

  _kubeapps_fqdn="$(onegate vm show --json | jq -rc '.VM.USER_TEMPLATE.KUBEAPPS_DNS_NAME')"
  _cert_domain="$(echo ${_kubeapps_fqdn} | cut -f2- -d.)"
  _cert_name="$(echo ${_cert_domain} | sed -e 's/\./-/g')"

  cat >"${1}" <<__EOF__
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: k3s-eole-local
  namespace: kube-system
spec:
  # Secret names are always required.
  secretName: k3s-eole-local-tls
  secretTemplate:
    annotations:
      my-secret-annotation-1: "k3s-eole-local certificate"
    labels:
      my-secret-label: k3s-eole-local

  duration: 2160h # 90d
  renewBefore: 360h # 15d
  subject:
    organizations:
      - eole
  commonName: k3s-eole.local
  isCA: false
  privateKey:
    algorithm: RSA
    encoding: PKCS1
    size: 2048
  usages:
    - server auth
    - client auth
  dnsNames:
    - k3s-eole.com
    - "*.k3s-eole.com"
  uris:
    - spiffe://cluster.local/ns/sandbox/sa/example
  ipAddresses:
    - 192.168.230.234
    - 192.168.0.102
  issuerRef:
    name: selfsigned-issuer
    kind: Issuer
__EOF__

}

waitForAllNodes() {
  tmout=${1}
  agents=${2}

  echo "[INFO] Waiting for ${agents} kubernetes nodes !"

  while [ "${tmout}" -ne 0 ]; do
    sleep 1
    nbAgents=$(kubectl get nodes | awk '/.*agent.*/ {print $1}' | wc -l)
    if [ "${nbAgents}" -lt "${agents}" ]; then
      tmout=$((tmout - 1))
      if [ "${tmout}" -eq 0 ]; then
        return 2
      fi
    else
      break
    fi
  done

  nodes=$(onegate service show --json | jq -rc '.SERVICE.roles[] | select(.name == "agent") | .nodes[].vm_info.VM.ID')

  tmout=${1}
  while [ "${tmout}" -ne 0 ]; do
    sleep 1
    status=0
    for nid in ${nodes}; do
      nds=$(onegate vm show ${nid} --json | jq -rc '.VM.USER_TEMPLATE.NODE_NAME')
      sts=$(kubectl get node "${nds}" --no-headers | awk '{print $2}')
      if [ "$sts" != "Ready" ]; then
        status=$((status + 1))
      fi
    done
    if [ "${status}" -eq 0 ]; then
      break
    fi
    tmout=$((tmout - 1))
    if [ "${tmout}" -eq 0 ]; then
      return 2
    fi
  done
  return 0
}

# DEPRECATED: maybe you want to give option --wait to helm?
waitForAllPods() {
  tmout=${1}
  echo "[INFO] Waiting for all pods to be ready."
  while [ "${tmout}" -ne 0 ]; do
    sleep 1
    status=0
    for sts in $(kubectl get pods -n openebs | awk 'NR>1 {print $3}'); do
      if [ "$sts" != "Running" ]; then
        status=$((status + 1))
      fi
    done
    if [ "${status}" -eq 0 ]; then
      break
    fi
    if [ "${tmout}" -eq 0 ]; then
      return 2
    else
      tmout=$((tmout - 1))
    fi
  done
  return 0
}

# Upgrade Traefik with a newer version
upgradeTraefik() {
  echo "[INFO] Upgrading Traefik helm chart."

  _replicas=${1}

  additionalArguments="additionalArguments={"
  traefikHelmChartVersion="10.19.5"

  additionalArguments="${additionalArguments}--api.dashboard=true,"
  additionalArguments="${additionalArguments}--api.insecure=true,"
  additionalArguments="${additionalArguments}--log.level=INFO,"
  additionalArguments="${additionalArguments}--providers.kubernetesingress.ingressclass=traefik-internal,"
  additionalArguments="${additionalArguments}--serversTransport.insecureSkipVerify=true}"

  if ! helm upgrade --install traefik \
    --namespace kube-system \
    --set deployment.replicas=${_replicas} \
    --set dashboard.enabled=true \
    --set rbac.enabled=true \
    --set logs.access.enabled=true \
    --set nodeSelector.node-type=master \
    --set="${additionalArguments}" \
    --wait --timeout=20m \
    traefik/traefik --version ${traefikHelmChartVersion}; then
    return 1
  fi

  # Work around https://github.com/traefik/traefik-helm-chart/issues/482
  kubectl apply -f https://raw.githubusercontent.com/traefik/traefik-helm-chart/master/traefik/crds/middlewarestcp.yaml
}

installOpenEBS() {
  echo "[INFO] Installing openEBS helm chart."
  # Manage repos

  helm install openebs \
    openebs/openebs \
    --namespace openebs \
    --create-namespace \
    --set cstor.enabled=true \
    --set image.repository="${PROXYHUB}" \
    --set apiserver.image="/proxyhub/openebs/m-apiserver" \
    --set cleanup.image.repository="/proxyhub/bitnami/kubectl" \
    --set cstor.pool.image="/proxyhub/openebs/cstor-pool" \
    --set cstor.poolMgmt.image="/proxyhub/openebs/cstor-pool-mgmt" \
    --set cstor.target.image="/proxyhub/openebs/cstor-istgt" \
    --set cstor.volumeMgmt.image="/proxyhub/openebs/cstor-volume-mgmt" \
    --set helper.image="/proxyhub/openebs/linux-utils" \
    --set jiva.image="/proxyhub/openebs/jiva" \
    --set localprovisioner.image="/proxyhub/openebs/provisioner-localpv" \
    --set ndm.image="/proxyhub/openebs/node-disk-manager" \
    --set ndmOperator.image="/proxyhub/openebs/node-disk-operator" \
    --set policies.monitoring.image="/proxyhub/openebs/m-exporter" \
    --set provisioner.image="/proxyhub/openebs/openebs-k8s-provisioner" \
    --set snapshotOperator.controller.image="/proxyhub/openebs/snapshot-controller" \
    --set snapshotOperator.provisioner.image="/proxyhub/openebs/snapshot-provisioner" \
    --set webhook.image="/proxyhub/openebs/admission-server" \
    --wait --timeout=10m 2>&1
}

installKubeApps() {
  echo "[INFO] Installing kubeapps helm chart."

  #_eole_repo_

  # Manage repos

  _dns_name=$(onegate vm show --json | jq -rc '.VM.USER_TEMPLATE.KUBEAPPS_DNS_NAME')
  if [ "${_dns_name}" = "null" ]; then
    echo "[ERROR] No dns name found for kubeapps service."
    return 1
  fi

  if ! helm install kubeapps \
    --namespace kubeapps \
    --create-namespace \
    --set global.imageRegistry="${PROXYHUB}/proxyhub" \
    --set ingress.enabled="true" \
    --set ingress.hostname="${_dns_name}" \
    --set ingress.certManager="true" \
    --set postgresql.persistence.enabled="true" \
    --set redis.enabled="true" bitnami/kubeapps; then
    echo "[ERROR] Kubeapps chart install failed !"
    return 2
  fi

  if ! kubectl create --namespace default serviceaccount kubeapps-operator; then
    echo "[ERROR] Service account creation failed!"
    return 3
  fi

  if ! kubectl create clusterrolebinding kubeapps-operator --clusterrole=cluster-admin --serviceaccount=default:kubeapps-operator; then
    echo "[ERROR] Role binding failed for kubeapps-operator !"
    return 4
  fi

  if ! _secret=$(kubectl get --namespace default serviceaccount kubeapps-operator -o jsonpath='{range .secrets[*]}{.name}{"\n"}{end}' | grep kubeapps-operator-token); then
    echo "[ERROR] Error retriving secret for kubeapps-operator"
    return 5
  fi

  kubectl get --namespace default secret "${_secret}" \
    -o jsonpath='{.data.token}' \
    -o go-template='{{.data.token | base64decode}}' | tee -a /root/.kubeapps-operator.secret
  echo >>/root/.kubeapps-operator.secret

}

installCertManager() {
  echo "[INFO] Installing CertManager"
  _cversion="1.8.0"
  _cnf=$(mktemp cert-manager-XXX.yaml)
  #_deploy_crds="https://github.com/cert-manager/cert-manager/releases/download/v${_cversion}/cert-manager.crds.yaml"

  #if ! kubectl apply -f ${_deploy_crds}; then
  #  echo "[ERROR} CRD Creation for cert-manager failed !"
  #  return 1
  #fi

  if ! helm install cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --create-namespace \
    --version ${_cversion} \
    --set installCRDs=true; then
    echo "[ERROR] cert-manager helm install failed :"
    return 1
  fi

  generateCertManagerConf "${_cnf}"

  kubectl apply -f "${_cnf}"

}

setupHelmRepos() {
  echo "[INFO] Setup Helm repositories"

  helm repo add openebs https://openebs.github.io/charts
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm repo add jetstack https://charts.jetstack.io
  helm repo add traefik https://helm.traefik.io/traefik

  helm repo update

}

labelAgentNodes() {
  echo "[INFO] Labeling agent nodes."
  ROLE="node-role.kubernetes.io/agent"
  nds=$(kubectl get nodes | awk '/<none>/ {print $1}')
  res=0
  for nd in ${nds}; do
    rl=$(echo "${nd}" | awk -F '-' '{print $1$2}')
    if ! kubectl label node "${nd}" "${ROLE}=${rl}"; then
      echo "[ERROR] Labeling failed for node ${rl}"
      res=$((res + 1))
    fi
    if ! kubectl label node "${nd}" "node-type=agent"; then
      echo "[ERROR] Labeling 'node-type' failed for node ${rl}"
      res=$((res + 1))
    fi
  done
  return ${res}
}
# Adding node-type label to the "masters"
labelServerNodes() {
  echo "[INFO] Labeling master nodes."
  nds=$(kubectl get nodes | awk '/master/ {print $1}')
  res=0
  for nd in ${nds}; do
    rl=$(echo "${nd}" | awk -F '-' '{print $1$2}')
    if ! kubectl label node "${nd}" "node-type=master"; then
      echo "[ERROR] Labeling failed for node ${rl}"
      res=$((res + 1))
    fi
  done
  return ${res}
}

# Install nginx ingress
setupNgninxIgress() {
  echo "[INFO] Installing Nginx ingress controler"

  helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
  helm repo update

  helm install ingress-controller ingress-nginx/ingress-nginx --namespace ingress --create-namespace
}

# Install completion for kubectl and helm
installCompletion() {
  echo "[INFO] Installing completion for kubectl and helm"

  kubectl completion bash >/usr/share/bash-completion/completions/kubectl
  helm completion bash >/usr/share/bash-completion/completions/helm
}

INGRESS_PROVIDER=$(onegate vm show --json | jq -cr .VM.USER_TEMPLATE.INGRESS_PROVIDER)

_start_log ${ONE_SERVICE_LOGDIR}/postdeploy.log

if ! waitForAllNodes 300 4; then
  echo "[ERROR] Some kubernetes nodesnot Ready or timeout (300s) is reached..."
  exit 1
fi

if ! labelAgentNodes; then
  echo "[ERROR] Nodes labeling failed..."
  exit 2
fi

if ! labelServerNodes; then
  echo "[ERROR}] Server nodes labeling failed..."
  exit 2
fi

if ! setupHelmRepos; then
  echo "[ERROR] Helm repository setup failed !"
  _end_log
  exit 7
fi

if [ "${INGRESS_PROVIDER}" = "nginx" ]; then
  if ! setupNgninxIgress; then
    echo "[ERROR] Nginx ingress installation failed !"
    _end_log
    exit 2
  fi
elif [ "${INGRESS_PROVIDER}" = "traefik" ]; then
  if ! upgradeTraefik 3; then
    echo "[ERROR] Traefik installation failed !"
    _end_log
    exit 8
  fi
else
  echo "[INFO] Don't install any ingress"
fi

# FIXME: For the moment, don't install anything more because not everyone
# want OpenEBS, kubeapps, etc... Keep the cluster (almost) naked.

#if ! installOpenEBS; then
#  echo "[ERROR] OpenEBS installation failed !"
#  _end_log
#  exit 3
#fi
#
#_cspcConf=$(mktemp cspc-XXXXX.yaml)
#generateCSPCconf "${_cspcConf}"
#if ! kubectl apply -f "${_cspcConf}"; then
#  echo "[ERROR] CSPC configuration application failed check ${_cspcConf}."
#  _end_log
#  exit 5
#else
#  rm -rf "${_cspcConf}"
#fi
#
#_storageClassConf=$(mktemp storageClass-XXXX.yaml)
#generateStorageClassConf "${_storageClassConf}"
#if ! kubectl apply -f "${_storageClassConf}"; then
#  echo "[ERROR] Class storage creation failed !"
#  _end_log
#  exit 6
#fi
#rm -rf _storageClassConf
#
#_csiDiskConf=$(mktemp cstor-csi-disk-XXXX.yaml)
#generatecStorCSIDiskConf "${_csiDiskConf}"
#kubectl apply -f "${_csiDiskConf}"
#if [ "$?" -ne 0 ]; then
#  echo "[ERROR] cStor CSI disk creation failed !"
#  _end_log
#  exit 2
#fi
#rm -rf _csiDiskConf
#
#_csiDiskConf=$(mktemp cstor-csi-disk-XXXX.yaml)
#generatecStorCSIDiskConf "${_csiDiskConf}"
#kubectl apply -f "${_csiDiskConf}"
#if [ "$?" -ne 0 ]; then
#  echo "[ERROR] cStor CSI disk creation failed !"
#  _end_log
#  exit 2
#fi
#rm -rf _csiDiskConf
#
#if ! installCertManager; then
#  echo "[ERROR] CertManager installation failed"
#  _end_log
#  exit 1
#fi
#
#if ! installKubeApps; then
#  echo "[ERROR] Kubeapps installation failed !"
#  _end_log
#  exit 7
#fi

if ! installCompletion; then
  echo "[ERROR] Autocompletion setup failed !"
  _end_log
  exit 2
fi

_end_log
exit 0
