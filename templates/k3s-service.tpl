{
  "name": "k3s-Cluster-RP",
  "deployment": "straight",
  "description": "Cluster K3S avec reverse proxy",
  "roles": [
    {
      "name": "agent",
      "cardinality": 4,
      "vm_template": 9,
      "shutdown_action": "terminate",
      "parents": [
        "server",
        "leader"
      ],
      "vm_template_contents": "NIC = [\n  NAME = \"NIC1\",\n  NETWORK_ID = \"$internal\" ]\n",
      "elasticity_policies": [],
      "scheduled_policies": []
    },
    {
      "name": "server",
      "cardinality": 2,
      "vm_template": 9,
      "parents": [
        "leader"
      ],
      "vm_template_contents": "NIC = [\n  NAME = \"NIC1\",\n  NETWORK_ID = \"$internal\" ]\n",
      "min_vms": 1,
      "max_vms": 4,
      "elasticity_policies": [],
      "scheduled_policies": []
    },
    {
      "name": "reverseProxy",
      "cardinality": 1,
      "vm_template": 12,
      "vm_template_contents": "NIC = [\n  NAME = \"NIC0\",\n  NETWORK_ID = \"$main\" ]\nNIC = [\n  NAME = \"NIC1\",\n  NETWORK_ID = \"$internal\" ]\n",
      "elasticity_policies": [],
      "scheduled_policies": []
    },
    {
      "name": "leader",
      "cardinality": 1,
      "vm_template": 9,
      "vm_template_contents": "NIC = [\n  NAME = \"NIC1\",\n  NETWORK_ID = \"$internal\" ]\n",
      "min_vms": 1,
      "max_vms": 1,
      "elasticity_policies": [],
      "scheduled_policies": []
    }
  ],
  "networks": {
    "main": "M|network|Main  network| |id:",
    "internal": "M|network|Internal network| |id:"
  },
  "custom_attrs": {
    "KUBEAPPS_DNS_NAME": "M|text|DNS Name for kubeapps service| |kubeapps.k3s-eole.local",
    "DNS_DOMAIN": "M|text|DNS name for the k3s cluster| |k3s-eole.local",
    "LE_EMAIL": "M|text|Email | |"
  },
  "shutdown_action": "terminate",
  "ready_status_gate": true
}

{
  "name": "k3s-Cluster",
  "deployment": "straight",
  "description": "Cluster K3S",
  "roles": [
    {
      "name": "leader",
      "cardinality": 1,
      "vm_template": 9,
      "shutdown_action": "terminate",
      "vm_template_contents": "NIC = [\n  NAME = \"NIC0\",\n  NETWORK_ID = \"$main\",\n  RDP = \"YES\" ]\nNIC = [\n  NAME = \"NIC1\",\n  NETWORK_ID = \"$internal\" ]\n",
      "elasticity_policies": [],
      "scheduled_policies": []
    },
    {
      "name": "agent",
      "cardinality": 4,
      "vm_template": 9,
      "shutdown_action": "terminate",
      "parents": [
        "leader"
      ],
      "vm_template_contents": "NIC = [\n  NAME = \"NIC0\",\n  NETWORK_ID = \"$main\",\n  RDP = \"YES\" ]\nNIC = [\n  NAME = \"NIC1\",\n  NETWORK_ID = \"$internal\" ]\n",
      "elasticity_policies": [],
      "scheduled_policies": []
    }
  ],
  "networks": {
    "main": "M|network|Main  network| |id:",
    "internal": "M|network|Internal network| |id:"
  },
  "custom_attrs": {
    "KUBEAPPS_DNS_NAME": "M|text|DNS Name for kubeapps service| |kubeapps.k3s-eole.local",
    "LE_EMAIL": "M|text|Email | |"
  },
  "shutdown_action": "terminate",
  "ready_status_gate": true
}